import React, { useState } from 'react';
import { Stage, Layer, Text } from 'react-konva';
// import Floorplan from './Floorplan'
import { Button } from 'reactstrap';

const Items = () => {
    const [state, setstate] = useState({
        isDragging: false,
        x: 50,
        y: 50
    })

    const [canvas, setcanvas] = useState(null);

    const handleClick = () => {
        setcanvas(addChair);
    }

    const addChair = (<Layer>
        <Text
            text="I am a chair"
            x={state.x}
            y={state.y}
            draggable
            fill={state.isDragging ? 'red' : 'green'}
            onDragStart={() => {
                setstate({
                    isDragging: true
                });
            }}
            onDragEnd={e => {
                setstate({
                    isDragging: false,
                    x: e.target.x(),
                    y: e.target.y()
                });
            }}
        />
    </Layer>
    )

    return (
        <React.Fragment>

            <h1>Try to drag me</h1>
            <Stage width={window.innerWidth} height={window.innerHeight / 1.5}>
                {canvas}
            </Stage>
            <div>
                <h4> Furnitures </h4>
                <Button color="primary" onClick={handleClick}>Load the chair</Button>
            </div>
        </React.Fragment>
    )
}

export default Items
