import React from 'react'
import JSONData from '../data/floorplan-sample.json'

const Floorplan = () => {
    const Data = JSONData.layout;
    return (
        <React.Fragment>
            <svg>
                {Data.map((data, index) => {
                    if (data.tag === 'polygon') {
                        return <polygon id={data.id} points={data.points}>
                            {console.log(data.id)}</polygon>
                    } else if (data.tag === 'path') {
                        return <path id={data.id} d={data.d}>
                            {console.log(data.id)}</path>
                    }
                })}
            </svg>
        </React.Fragment>

    )
}

export default Floorplan
