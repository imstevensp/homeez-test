import React from 'react';
import { render } from 'react-dom';
import { Stage, Layer, Image } from 'react-konva';
import useImage from 'use-image';
import house from '../images/furniture-sample.svg'
import room1 from '../images/room1.png'
import room2 from '../images/room2.png'
import room3 from '../images/room3.jpg'

const URLImage = ({ image }) => {
    const [img] = useImage(image.src);
    return (
        <Image
            image={img}
            x={image.x}
            y={image.y}
            // I will use offset to set origin to the center of the image
            offsetX={img ? img.width / 2 : 0}
            offsetY={img ? img.height / 2 : 0}
        />
    );
};

const CanvasDragDrop = () => {
    const dragUrl = React.useRef();
    const stageRef = React.useRef();
    const [images, setImages] = React.useState([]);
    return (
        <div>
            Try to trag and image into the stage:
            <br />
            <img
                style={{ width: '30rem'}}
                alt="house"
                src={house}
                draggable="true"
                onDragStart={(e) => {
                    dragUrl.current = e.target.src;
                }}
            />
            <img
                style={{ width: '21rem'}}
                alt="room1"
                src={room1}
                draggable="true"
                onDragStart={(e) => {
                    dragUrl.current = e.target.src;
                }}
            />
            <img
                style={{ width: '24rem'}}
                alt="room2"
                src={room2}
                draggable="true"
                onDragStart={(e) => {
                    dragUrl.current = e.target.src;
                }}
            />
            <img
                style={{ width: '30rem'}}
                alt="room3"
                src={room3}
                draggable="true"
                onDragStart={(e) => {
                    dragUrl.current = e.target.src;
                }}
            />
            
            <div
                onDrop={(e) => {
                    e.preventDefault();
                    // register event position
                    stageRef.current.setPointersPositions(e);
                    // add image
                    setImages(
                        images.concat([
                            {
                                ...stageRef.current.getPointerPosition(),
                                src: dragUrl.current,
                            },
                        ])
                    );
                }}
                onDragOver={(e) => e.preventDefault()}
            >
                <Stage
                    width={window.innerWidth}
                    height={window.innerHeight}
                    style={{ border: '1px solid grey' }}
                    ref={stageRef}
                >
                    <Layer>
                        {images.map((image) => {
                            return <URLImage image={image} />;
                        })}
                    </Layer>
                </Stage>
            </div>
        </div>
    )
}

export default CanvasDragDrop
