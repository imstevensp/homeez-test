import './App.css';
import Items from './components/Items';
import Floorplan from './components/Floorplan';
import CanvasDragDrop from './components/CanvasDragDrop';

function App() {
  return (
    <div className="App">
      <h1>Floorplan Layout</h1>
      <Floorplan />
      <CanvasDragDrop />
      <Items>
      </Items>
    </div>
  );
}

export default App;
